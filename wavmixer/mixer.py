import scipy.io.wavfile
import numpy as np
import sys
import matplotlib.pyplot as plt
import math

def readFile(name):
    rate, data = scipy.io.wavfile.read(name)
    return rate, [d / 32768.0 for d in data[:,0]]  # only left channel samples

def mix(signals, clipFn, coeff):
    r   = signals[0]
    for s in signals[1:]:
        r   = clipFn(r * coeff + s)
    return r

def delaySignalBy(signal, rate, byTime):
    return np.concatenate((np.zeros(int(rate * byTime)), signal))

def unifyLengths(signals):
    maxLength   = max(len(s) for s in signals)
    return [np.concatenate((s, np.zeros(maxLength - len(s)))) for s in signals]

def plot(count, index, l, x, y):
    plt.subplot(count, 1, index, title=l)   # count rows, 1 column
    plt.plot(x, y)

def plotFFT(count, index, label, data, rate):
    fftPowers   = [abs(y) for y in np.fft.rfft(data)]
    k           = max(fftPowers[:-1])   # normalization coeff
    fftFreq     = np.fft.fftfreq(len(data), 1.0 / rate)
    plot(count, index, label, fftFreq[:len(fftPowers) - 1], [x / k for x in fftPowers[:-1]])

if len(sys.argv) < 2:
    print "usage:", sys.argv[0], "<filename> [-n]"
    sys.exit(1)

rate, data = readFile(sys.argv[1])

inputs = unifyLengths([
    delaySignalBy(data, rate, 0.0),
    delaySignalBy(data, rate, 0.5 / 7),
    delaySignalBy(data, rate, 2 * 0.5 / 7),
    delaySignalBy(data, rate, 3 * 0.5 / 7),
    delaySignalBy(data, rate, 4 * 0.5 / 7),
    delaySignalBy(data, rate, 5 * 0.5 / 7),
])

signals = [
    inputs[0], inputs[0], inputs[0],
    inputs[1], inputs[1], inputs[1],
    inputs[2], inputs[2], inputs[2],
    inputs[3], inputs[3], inputs[3],
]

sampleCount = len(signals[0])

mixed = [
    ("no clip", mix(signals, lambda x: x, 1.0)),
    ("hard clip", mix(signals, np.vectorize(lambda x: x if math.fabs(x) <= 1.0 else x/math.fabs(x)), 1.0)),
    ("tanh on every sum", mix(signals, np.tanh, 1.0)),
    ("tanh at the end", np.tanh(mix(signals, lambda x: x, 1.0)))
]

plt.figure(1)
if len(sys.argv) > 2 and sys.argv[2] == "-n":
    x   = np.linspace(0.0, float(sampleCount) / rate, sampleCount)
    for i in range(0, 4):
        plot(len(mixed), i + 1, mixed[i][0], x, mixed[i][1])
else:
    for i in range(0, 4):
        plotFFT(len(mixed), i + 1, mixed[i][0], mixed[i][1], rate)
plt.show()