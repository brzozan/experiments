from Tkinter import *
from numpy import array
from math import *

screen_width = 480 # physical width
screen_height = 640 # physical height
aspect_ratio = float(screen_width) / screen_height
width = 10.0 # logical width
height = width / aspect_ratio # logical height
radius = 0.2 # ball radius
fov = 60.0 * 2.0 / pi # camera's field of view

master = Tk()
canvas = Canvas(master, width=screen_width, height=screen_height)

def screenToLogical(x, y):
    return array([x * width / screen_width, y * height / screen_height])

def logicalToScreen(x, y):
    return (x / width * screen_width, y / height * screen_height)

camera = array([width / 2.0, 0.05 * height])
balls = [array([width / 2.0, 0.25 * height])]

def drawBall(x, y):
    xs, ys = logicalToScreen(x, y)
    rx, ry = logicalToScreen(radius, radius)
    canvas.create_oval(xs - rx, ys - ry, xs + rx, ys + ry)

def drawCamera():
    xs, ys = logicalToScreen(camera[0], camera[1])
    canvas.create_line(xs, ys, xs + screen_height * sin(fov), screen_height, fill="#808080")
    canvas.create_line(xs, ys, xs - screen_height * sin(fov), screen_height, fill="#808080")
    canvas.create_line(xs, ys, xs, screen_height, fill="#c0c0c0")

draggedBallIndex = -1

def mousePressed(event):
    global draggedBallIndex
    pos = screenToLogical(event.x, event.y)
    for i in range(0, len(balls)):
        d = sqrt(sum((pos - balls[i]) ** 2))
        if d < radius:
            draggedBallIndex = i
            break

def mouseReleased(event):
    global draggedBallIndex
    draggedBallIndex = -1
    
def mouseMotion(event):
    global draggedBallIndex
    if draggedBallIndex >= 0 and draggedBallIndex < len(balls):
        pos = screenToLogical(event.x, event.y)
        balls[draggedBallIndex] = pos

def addBall(event):
    pos = screenToLogical(event.x, event.y)
    balls.append(pos)

def removeBall(event):
    pos = screenToLogical(event.x, event.y)
    for i in range(0, len(balls)):
        # naive dragging - if mouse moves out of the ball during
        # movement dragging will stop
        d = sqrt(sum((pos - balls[i]) ** 2))
        if d < radius:
            balls.pop(i)
            break

def total3(x, p):
    def distFun(x, a):
        return ((a[0] - x) / a[1])
    k = map(lambda pp: distFun(x, pp), p)
    return min(k) + max(k)

def total2(x, p):
    def distFun(x, a):
        return (a[0] - x)
    k = map(lambda pp: distFun(x, pp), p)
    return min(k) + max(k)

def total1(x, p):
    def distFun(x, a):
        return (a[0] - x)
    k = map(lambda pp: distFun(x, pp), p)
    return sum(k)

total = total1

def update():
    canvas.delete('all')
    for b in balls:
        drawBall(b[0], b[1])
    updateCamera()
    drawCamera()
    master.after(20, update)

def updateCamera():
    global total
    camera[0] = camera[0] + 0.1 * total(camera[0], balls)

def switchAlgorithm(event):
    global total
    if event.char == '1':
        total = total1
    elif event.char == '2':
        total = total2
    elif event.char == '3':
        total = total3

master.bind('<Button-1>', mousePressed)
master.bind('<ButtonRelease-1>', mouseReleased)
master.bind('<B1-Motion>', mouseMotion) # motion with left button pressed
master.bind('<Double-Button-1>', addBall)
master.bind('<Button-2>', removeBall)
master.bind('1', switchAlgorithm)
master.bind('2', switchAlgorithm)
master.bind('3', switchAlgorithm)
update()
canvas.pack()
mainloop()
